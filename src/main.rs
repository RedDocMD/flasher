use clap::{App, Arg, ArgGroup};
use std::{
    fs, io,
    os::unix::fs::FileTypeExt,
    path::{Path, PathBuf},
    process::{self, Command, Stdio},
    thread,
    time::Duration,
};
use thiserror::Error;

fn main() {
    let app = get_app();
    let matches = app.get_matches();
    let res = if matches.is_present("usb") {
        flash_usb(matches.value_of("INPUT").unwrap())
    } else if matches.is_present("swd") {
        flash_swd(matches.value_of("INPUT").unwrap())
    } else {
        unreachable!("expected clap to require exactly one of usb or swd but not none");
    };
    if let Err(err) = res {
        eprintln!("{}", err);
        process::exit(1);
    }
}

fn get_app() -> App<'static, 'static> {
    App::new("flasher")
        .version("0.1.0")
        .author("Deep Majumder <deep.majumder2019@gmail.com>")
        .about("Flashes programs onto Raspberry Pi Pico")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to flash")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("swd")
                .long("swd")
                .short("s")
                .help("Flash by mounting Pico as USB mass-storage device"),
        )
        .arg(
            Arg::with_name("usb")
                .long("usb")
                .short("u")
                .help("Flash by SWD pins of Pico"),
        )
        .group(
            ArgGroup::with_name("mode")
                .args(&["swd", "usb"])
                .required(true),
        )
}

fn flash_usb<P: AsRef<Path>>(inp_path: P) -> Result<(), FlasherError> {
    let dev_sda = PathBuf::from("/dev/sda1");
    while !dev_sda.exists() {
        println!("Waiting until /dev/sda1 is found ...");
        thread::sleep(Duration::from_millis(2500));
    }
    let dev_sda_filetype = fs::metadata(&dev_sda)?.file_type();
    if !dev_sda_filetype.is_block_device() {
        return Err(FlasherError::NotBlockDevice("/dev/sda1"));
    }

    let rel_wait = Duration::from_secs(2);
    println!(
        "Waiting for {}s for you to release the BOOTSEL button",
        rel_wait.as_secs()
    );
    thread::sleep(rel_wait);

    Command::new("sudo")
        .args(&["mkdir", "-p", "/mnt/pico"])
        .output()?;

    Command::new("sudo")
        .args(&["mount", "/dev/sda1", "/mnt/pico"])
        .stdin(Stdio::inherit())
        .output()?;

    Command::new("sudo")
        .args(&["cp", &inp_path.as_ref().display().to_string(), "/mnt/pico"])
        .output()?;

    Command::new("sudo").args(&["sync"]).output()?;
    Command::new("sudo")
        .args(&["umount", "/mnt/pico"])
        .output()?;
    Ok(())
}

fn flash_swd<P: AsRef<Path>>(inp_path: P) -> Result<(), FlasherError> {
    todo!("implement flash_swd")
}

#[derive(Error, Debug)]
enum FlasherError {
    #[error("{0}")]
    IOError(#[from] io::Error),
    #[error("{0} is not a block device")]
    NotBlockDevice(&'static str),
}
